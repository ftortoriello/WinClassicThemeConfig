@ECHO OFF

REM Check for Admin privileges
fltmc.exe >NUL 2>&1
IF NOT ERRORLEVEL 1 GOTO :Begin

REM Create script to run me elevated
ECHO Administrator privileges are needed to register programs in the Control Panel.
ECHO If a User Account Control prompt is shown, press Yes to continue.
(
	ECHO Set App = CreateObject^("Shell.Application"^)
	ECHO App.ShellExecute "%~nx0", "", "%~dp0", "runas", 1
) > "%TEMP%\RunAsAdmin.vbs"
cscript /nologo "%TEMP%\RunAsAdmin.vbs"
DEL "%TEMP%\RunAsAdmin.vbs"
EXIT /B 1


:Begin
TITLE Themes.exe Control Panel registration

SET exepath=%%SystemDrive%%\Programs\Classic\Themes.exe
CALL :PromptPath

REM Task links are supported from Windows Vista
SET xmlpath=
REM Check for version numbers from Vista to 11
VER | FIND "6." >NUL
IF ERRORLEVEL 1 VER | FIND "10." >NUL
IF NOT ERRORLEVEL 1 (
	SET xmlpath=%exepath:~0,-4%.xml
)

SET uuid=B17C439B-A9B9-4D46-B914-103423B2926C
SET name=ClassicAppearance

ECHO Registering Control Panel item...
CALL :AddReg
IF "%PROCESSOR_ARCHITECTURE%"=="AMD64" (
	CALL :AddReg /Reg:32
)
ECHO.

IF NOT "%xmlpath%"=="" (
	ECHO Creating Control Panel task links file...
	CALL :CreateTasksXml
	ECHO.
)

REM Register theme and modern style extensions from XP to 11
VER | FIND "5.1" >NUL
IF ERRORLEVEL 1 VER | FIND "5.2" >NUL
IF ERRORLEVEL 1 VER | FIND "6." >NUL
IF ERRORLEVEL 1 VER | FIND "10." >NUL
IF NOT ERRORLEVEL 1 (
	ECHO Registering file extensions...
	CALL :RegisterExt
	ECHO.
)

ECHO Finished.
PAUSE
GOTO :EOF


:PromptPath
SET /P exepath=Enter the "Themes.exe" absolute file path [%exepath%]: 
REM Expand path
FOR /F "tokens=*" %%P IN ('ECHO %exepath%') DO SET realpath=%%P
IF NOT "%realpath:~1,1%"==":" (
	ECHO The path must be absolute.
	ECHO.
	GOTO :PromptPath
)
IF NOT "%realpath:~-4%"==".exe" (
	SET exepath=%exepath%.exe
	SET realpath=%realpath%.exe
)
IF NOT EXIST "%realpath%" (
	ECHO The file %realpath% does not exist.
	ECHO.
	GOTO :PromptPath
)
ECHO.
GOTO :EOF


:AddReg
SET key=HKLM\SOFTWARE\Classes\CLSID\{%uuid%}
reg.exe ADD %key% /ve /t REG_SZ /d %name% /f %*
reg.exe ADD %key% /v LocalizedString /t REG_EXPAND_SZ /d "@%exepath%,-41" /f %*
reg.exe ADD %key% /v InfoTip         /t REG_EXPAND_SZ /d "@%exepath%,-42" /f %*
reg.exe ADD %key% /v System.ApplicationName /t REG_SZ /d "%name%" /f %*
reg.exe ADD %key% /v System.ControlPanel.Category /t REG_SZ /d "1,7" /f %*
reg.exe ADD %key% /v System.ControlPanel.EnableInSafeMode /t REG_DWORD /d 3 /f %*
IF NOT "%xmlpath%"=="" (
	reg.exe ADD %key% /v System.Software.TasksFileUrl /t REG_EXPAND_SZ /d "%xmlpath%" /f %*
)

reg.exe ADD %key%\DefaultIcon /ve /t REG_EXPAND_SZ /d "%exepath%,-100" /f %*
reg.exe ADD %key%\Shell\Open\Command /ve /t REG_EXPAND_SZ /d "%exepath%" /f %*
reg.exe ADD %key%\ShellFolder /v Attributes /t REG_DWORD /d 0 /f %*

SET key=HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace\{%uuid%}
reg.exe ADD %key% /ve /t REG_SZ /d %name% /f %*
GOTO :EOF


:CreateTasksXml
REM Expand path
FOR /F "tokens=*" %%P IN ('ECHO %xmlpath%') DO SET outpath=%%P

SET uuidcls=33E2A59A-E1FA-45BE-8BF2-B0FD728945FA
SET uuideff=76FC2B46-280C-4BF2-AD0B-6BC444B723AD
(
	ECHO ^<?xml version="1.0" ?^>
	ECHO ^<applications xmlns="http://schemas.microsoft.com/windows/cpltasks/v1"
	ECHO 	xmlns:sh="http://schemas.microsoft.com/windows/tasks/v1"^>
	ECHO.
	ECHO ^<application id="{%uuid%}"^>
	ECHO.
	ECHO ^<!-- Classic Style --^>
	ECHO ^<sh:task id="{%uuidcls%}"^>
	ECHO 	^<sh:name^>@%exepath%,-1243^</sh:name^>
	ECHO 	^<sh:keywords^>@%exepath%,-1253^</sh:keywords^>
	ECHO 	^<sh:command^>%exepath% @Appearance^</sh:command^>
	ECHO ^</sh:task^>
	ECHO.
	ECHO ^<!-- Effects --^>
	ECHO ^<sh:task id="{%uuideff%}"^>
	ECHO 	^<sh:name^>@%exepath%,-1245^</sh:name^>
	ECHO 	^<sh:keywords^>@%exepath%,-1255^</sh:keywords^>
	ECHO 	^<sh:command^>%exepath% @Effects^</sh:command^>
	ECHO ^</sh:task^>
	ECHO.
	ECHO ^<!-- Appearance and Personalization category --^>
	ECHO ^<category id="1"^>
	ECHO 	^<sh:task idref="{%uuidcls%}"/^>
	ECHO 	^<sh:task idref="{%uuideff%}"/^>
	ECHO ^</category^>
	ECHO.
	ECHO ^<!-- Ease of Access category --^>
	ECHO ^<category id="7"^>
	ECHO 	^<sh:task idref="{%uuidcls%}"/^>
	ECHO 	^<sh:task idref="{%uuideff%}"/^>
	ECHO ^</category^>
	ECHO.
	ECHO ^</application^>
	ECHO ^</applications^>
) > "%outpath%"
GOTO :EOF

:RegisterExt
SET key=HKLM\SOFTWARE\Classes\%name%
reg.exe ADD %key% /ve /t REG_SZ /d "%name%" /f %*
reg.exe ADD %key%\DefaultIcon /ve /t REG_SZ /d "ThemeUI.dll,-701" /f %*
reg.exe ADD %key%\Shell /ve /t REG_SZ /d "Open" /f %*
reg.exe ADD %key%\Shell\Open\Command /ve /t REG_EXPAND_SZ /d "\"%exepath%\" \"%%1\" %*" /f %*

SET key=HKLM\SOFTWARE\Classes\Applications\Themes.exe

REM Windows by default does not let you associate file types to programs named
REM Themes.exe (it includes a "NoOpenWith" value). Delete the key.
reg.exe DELETE %key% /f %* 2>NUL

reg.exe ADD %key% /v FriendlyAppName /t REG_EXPAND_SZ /d "@%exepath%,-100" /f %*
reg.exe ADD %key%\DefaultIcon /ve /t REG_SZ /d "ThemeUI.dll,-701" /f %*
reg.exe ADD %key%\Shell\Open\Command /ve /t REG_EXPAND_SZ /d "\"%exepath%\" \"%%1\" %*" /f %*
FOR %%E IN ( .theme .msstyles ) DO (
	reg.exe ADD HKLM\SOFTWARE\Classes\%%E\OpenWithProgids /v "%name%" /t REG_SZ /d "" /f %*
	reg.exe ADD %key%\SupportedTypes /v %%E /t REG_SZ /d "" /f %*
)
GOTO :EOF

