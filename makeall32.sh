#!/bin/sh

if [ -z "$MAKE" ] ; then
	MAKE=mingw32-make
	command -v $MAKE 2&>/dev/null || MAKE=make
fi

function _make()
{
	winver="$1"
	winverpe="$2"
	winvername="$3"
	shift 3

	$MAKE \
		CPPFLAGS="-DNDEBUG -DWINVER=$winver $CPPFLAGS" \
		CFLAGS="-O2 $CFLAGS" \
		LDFLAGS="-Wl,-subsystem,windows:$winverpe $LDFLAGS" $@

	mkdir -p out/$winvername
	[ -f Themes.exe ] && mv Themes.exe out/$winvername/Themes.exe

	$MAKE clean
}

$MAKE clean

export CFLAGS='-march=pentium'
_make '0x0400' '4.0' '04-95' $@

export LDFLAGS='-lmsimg32 -lshlwapi'
_make '0x0500' '4.0' '04-98,ME' $@

export CPPFLAGS='-DUNICODE'
export LDFLAGS=
_make '0x0400' '4.0' '04-NT4' $@

export LDFLAGS='-lshlwapi'
_make '0x0500' '5.0' '05-2000' $@

export CFLAGS=
export CPPFLAGS=
export LDFLAGS='-lshlwapi -luxtheme'
_make '0x0501' '5.1' '05-XP' $@

export LDFLAGS='-lshlwapi' #-luxtheme -ldwmapi
_make '0x0600' '6.0' '06-Vista' $@

#_make '0x0601' '6.1' '07-x86' $@

export LDFLAGS='-lshlwapi -luxtheme -lole32 -luuid'
#_make '0x0603' '6.2' '08-x86' $@

_make '0x0A00' '6.2' '10-x86' $@

export CPPFLAGS='-DWINE'
_make '0x0A00' '6.2' 'Wine-x86' $@

export CPPFLAGS='-DMINIMAL'
export LDFLAGS='-lshlwapi'
_make '0x0A00' '6.2' '10-Minimal' $@
