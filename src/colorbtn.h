#pragma once

#include "config.h"

#if !defined(COLORBTN_H) && defined(WITH_CLASSIC)
#define COLORBTN_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

HBITMAP CreateColorButtonBitmap(HWND hButton, _In_ RECT *lpRcBmp);

void FillColorButton(HWND hButton, _In_ const RECT *lpRcBmp,
    HBITMAP hbmpColor, HBRUSH hBrush);

void DrawColorButtonBorder(HWND hButton, _In_ const RECT *lpRcBmp,
    HBITMAP hbmpColor, HBRUSH hBrush);

void DisableColorButton(HWND hButton);

BOOL ChooseCustomColor(_Inout_ COLORREF *crColor, HWND hwndOwner);

#endif  /* !defined(COLORBTN_H) && defined(WITH_CLASSIC) */
