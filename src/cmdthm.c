/*
 * COPYRIGHT: See COPYING in the top level directory
 * PURPOSE:   Themes command line handling
 *
 * PROGRAMMER: Franco Tortoriello (torto09@gmail.com)
 */

#include "app.h"

#if defined(WITH_CMDLINE) && defined(WITH_THEMES)

#include "thm.h"
#include "resource.h"
#include "util.h"
#if WINVER >= WINVER_XP
#include "uxtundoc.h"
#endif

#include <shlwapi.h>

#if WINVER >= WINVER_XP && defined(UNICODE)
_Success_(return)
static
BOOL ApplyMsStyles(_In_z_ const TCHAR *path)
{
    if (!LoadUxThemeFunctions())
    {
        PrintResource(IDS_ERROR_UXTHEME);
        return RETURN_ERROR;
    }

    HRESULT hr;

#if WINVER >= WINVER_8 && !defined(WINE)
    SCHEME_DATA schemeData;
    LoadCurrentScheme(&schemeData);

    hr = ApplyVisualStyle(path, NULL, NULL, schemeData.colors);
    ApplyScheme(&schemeData, NULL);
#else
    hr = ApplyVisualStyle(path, NULL, NULL);
#endif

    UnloadUxThemeFunctions();
    if (hr == S_OK)
        return TRUE;

    ShowApplyThemeStyleError(hr);
    return FALSE;
}
#endif

_Success_(return != RETURN_ERROR)
UINT ApplyThemeFromCommandLine(_In_z_ WCHAR *filePathW)
{
#if defined UNICODE
#define filePath filePathW
#else
    TCHAR filePath[MAX_PATH];
    WideCharToMultiByte(CP_ACP, 0, filePathW, -1, filePath,
        MAX_PATH * sizeof(TCHAR), NULL, NULL);
#endif

    if (!PathFileExists(filePath))
    {
        PrintLastErrorMessage();
        return RETURN_ERROR;
    }

    TCHAR absolutePath[MAX_PATH];
    TCHAR *szFilePart;  /* Used to determine if the path is to a file or not */
    DWORD len = GetFullPathName(filePath, MAX_PATH, absolutePath, &szFilePart);
    if (len == 0)
    {
        PrintLastErrorMessage();
        return RETURN_ERROR;
    }
    if (len > MAX_PATH)
    {
        PrintResource(IDS_ERROR_MEM);
        return RETURN_ERROR;
    }
    if (!szFilePart)
    {
        PrintResource(IDS_FILEPATH_INVALID);
        return RETURN_ERROR;
    }

    /* .theme file? */
    if (ApplyTheme(absolutePath))
        return RETURN_CHANGES;

#if WINVER >= WINVER_XP && defined(UNICODE)
    /* .msstyles file? */
    if (ApplyMsStyles(absolutePath))
        return RETURN_CHANGES;
#else
    PrintResource(IDS_ERROR_THEME);
#endif

    return RETURN_ERROR;

#undef filePath
}

#endif  /* defined(WITH_CMDLINE) && defined(WITH_THEMES) */
