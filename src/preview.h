#pragma once

#include "config.h"

#if !defined(PREVIEW_H) && defined(WITH_CLASSIC)
#define PREVIEW_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/* Preview messages */
#define PVM_SETSIZE            (WM_USER+1)
#define PVM_SETFONT            (WM_USER+2)
#define PVM_UPDATESCHEME       (WM_USER+3)

/* PVM_UPDATESCHEME flags */
#define PV_UPDATE_ONLY_COLORS        0x1L
#if WINVER >= WINVER_XP
#define PV_UPDATE_EXPLORER_SETTINGS  0x2L
#endif

_Success_(return)
BOOL RegisterSchemePreviewControl(void);

void UnregisterSchemePreviewControl(void);

#endif  /* !defined(PREVIEW_H) && defined(WITH_CLASSIC) */
