/*
 * COPYRIGHT: See COPYING in the top level directory
 * PURPOSE:   UxTheme undocumented function wrappers
 *
 * PROGRAMMER: Franco Tortoriello (torto09@gmail.com)
 */

#include "app.h"
#include "uxtundoc.h"

#if defined(WITH_THEMES) && WINVER >= WINVER_XP

#include <vssym32.h>

#if WINVER >= WINVER_VISTA
#define STRICT_TYPED_ITEMIDS
#include <shlobj.h>

#include <shlwapi.h>
#endif

static HMODULE g_hUxThemeDll = NULL;

#if defined(WITH_THEMES)

#if defined(WINE)
/* From XP to 7 */
static HRESULT(WINAPI *OpenThemeFile)(
    _In_z_     const WCHAR *pszStyleFile,
    _In_opt_z_ const WCHAR *pszColor,
    _In_opt_z_ const WCHAR *pszSize,
    _Out_ HTHEME *phTheme,
    BOOL fGlobal
) = NULL;
#endif

static HRESULT(WINAPI *GetThemeDefaults)(
    _In_z_ const WCHAR *pszStyleFile,
    _Out_writes_opt_z_(cchMaxColorChars) WCHAR *pszDefaultColor,
    INT cchMaxColorChars,
    _Out_writes_opt_z_(cchMaxSizeChars) WCHAR *pszDefaultSize,
    INT cchMaxSizeChars
) = NULL;

/* dwFlags = 1: Apply style metrics, and on XP, colors
 */
#if defined(WINE)
/* From XP to 7, but only works on XP */
static HRESULT(WINAPI *ApplyTheme)(
    _In_ HTHEME hTheme,
    DWORD dwFlags,
    _In_opt_ HWND hWnd
) = NULL;
#else
static HRESULT(WINAPI *SetSystemVisualStyle)(
    _In_z_     const WCHAR *pszStyleFile,
    _In_opt_z_ const WCHAR *pszColor,
    _In_opt_z_ const WCHAR *pszSize,
    DWORD dwFlags
) = NULL;
#endif

#if WINVER >= WINVER_8 && !defined(WINE)

/* Source:
 * https://github.com/KappaBar/KappaBar/blob/main/util/ThemeManager.h
 */
typedef struct tagUXTHEMEFILE
{
    CHAR    header[7];  /* "thmfile" */
    VOID   *sharableSectionView;
    HANDLE  hSharableSection;
    VOID   *nonSharableSectionView;
    HANDLE  hNonSharableSection;
    CHAR    end[3];     /* "end" */
} UXTHEMEFILE;

/* From 7 */
static HTHEME(WINAPI *OpenThemeDataFromFile)(
    _In_ UXTHEMEFILE *lpUxThemeFile,
    HWND hWnd,
    _In_opt_z_ const WCHAR *pszClassList,
    /* Removed on later Win10 versions, but appears to be harmless */
    BOOL fClient
) = NULL;

/* From 8 */
static HRESULT(WINAPI *LoaderLoadTheme)(
    _In_opt_ HTHEME hTheme,
    _In_opt_ HINSTANCE hInstance,
    _In_z_ const WCHAR *pszStyleFile,
    _In_z_ const WCHAR *pszColorParam,
    _In_z_ const WCHAR *pszSizeParam,
    _Out_ HANDLE *phSharableSection,
    _Out_writes_opt_z_(cchSharableSectionName) WCHAR *pszSharableSectionName,
    INT cchSharableSectionName,
    _Out_ HANDLE *phNonSharableSection,
    _Out_writes_opt_z_(cchNonSharableSectionName) WCHAR *pszNonSharableSectionName,
    INT cchNonSharableSectionName,
    _In_opt_ VOID *pfnCustomLoadHandler,
    _Out_opt_ HANDLE *phReuseSection,
    INT iCurrentScreenPpi,
    WORD wCurrentLangID,
    /* Removed on 2022, but appears to be harmless */
    BOOL fGlobal
) = NULL;

#endif  /* WINVER >= WINVER_8 && !defined(WINE) */

#endif  /* defined(WITH_THEMES) */

#if WINVER >= WINVER_8 && !defined(WINE)
typedef struct tagACCENTCOLORS
{
    COLORREF color1;
    COLORREF color2;
} ACCENTCOLORS;

static ACCENTCOLORS g_colors;

static HRESULT(WINAPI *GetUserColorPreference)
    (ACCENTCOLORS *pPreference, BOOL fForceReload) = NULL;
static HRESULT(WINAPI *SetUserColorPreference)
    (const ACCENTCOLORS *pPreference, BOOL fForceCommit) = NULL;
#endif

#if WINVER > WINVER_8 && defined(WITH_THEMES)
/* On Windows 8.1 and later, the system crashes when trying to apply a
 * .msstyles file which is inside the user folder, %UserProfile%.
 * Return FALSE in that case.
 */
static
BOOL IsValidVisualStylePath(_In_z_ const WCHAR *path)
{
    BOOL ret = FALSE;
    WCHAR *userPath;
    HRESULT hr = SHGetKnownFolderPath(&FOLDERID_Profile, 0, NULL, &userPath);
    if (hr != S_OK)
        return ret;

    ret = !PathIsPrefixW(userPath, path);

    CoTaskMemFree(userPath);
    return ret;
}
#endif

#if defined(WITH_THEMES)
/* Note that this returns FALSE if the paths differ in anything except case.
 */
static
BOOL IsCurrentVisualStyle(
    _In_z_ const WCHAR *pszNewFile,
    _In_z_ const WCHAR *pszNewColor,
    _In_z_ const WCHAR *pszNewSize)
{
    WCHAR pszCurrentFile[MAX_PATH];
    WCHAR pszCurrentColor[MAX_PATH];
    WCHAR pszCurrentSize[MAX_PATH];

    HRESULT hr = GetCurrentThemeName(
        pszCurrentFile, MAX_PATH,
        pszCurrentColor, MAX_PATH,
        pszCurrentSize, MAX_PATH);

    return hr == S_OK &&
        lstrcmpiW(pszCurrentFile, pszNewFile) == 0 &&
        lstrcmpiW(pszCurrentColor, pszNewColor) == 0 &&
        lstrcmpiW(pszCurrentSize, pszNewSize)  == 0;
}

#if WINVER >= WINVER_8 || defined(WINE)
_Success_(return != NULL)
static
HTHEME OpenMsStles(
    _In_z_ const WCHAR *pszStyleFile,
    _In_z_ const WCHAR *pszColor,
    _In_z_ const WCHAR *pszSize
#if WINVER >= WINVER_8 && !defined(WINE)
  , _Out_ UXTHEMEFILE *pUxThemeFile
#endif
)
{
    HTHEME hTheme;
    HRESULT hr;

#if WINVER >= WINVER_8 && !defined(WINE)

    HANDLE hSharable;
    HANDLE hNonSharable;

    hr = LoaderLoadTheme(NULL, NULL,
        pszStyleFile, pszColor, pszSize,
        &hSharable, NULL, 0,
        &hNonSharable, NULL, 0,
        NULL, NULL, 0, 0, FALSE);
    if (hr != S_OK)
        return NULL;

    memcpy(pUxThemeFile->header, "thmfile", _countof(pUxThemeFile->header));

    pUxThemeFile->sharableSectionView =
        MapViewOfFile(hSharable, FILE_MAP_READ, 0, 0, 0);
    pUxThemeFile->hSharableSection = hSharable;

    pUxThemeFile->nonSharableSectionView =
        MapViewOfFile(hNonSharable, FILE_MAP_READ, 0, 0, 0);
    pUxThemeFile->hNonSharableSection = hNonSharable;

    memcpy(pUxThemeFile->end, "end", _countof(pUxThemeFile->end));

    hTheme = OpenThemeDataFromFile(pUxThemeFile, NULL, NULL, FALSE);

#else  /* WINVER <= WINVER_7 || defined(WINE) */

    hr = OpenThemeFile(pszStyleFile, pszColor, pszSize, &hTheme, FALSE);
    if (hr != S_OK)
        return NULL;

#endif

    return hTheme;
}
#endif

#endif  /* defined(WITH_THEMES) */

_Success_(return)
BOOL LoadUxThemeFunctions(void)
{
    if (g_hUxThemeDll)
        return TRUE;  /* Already loaded */

    g_hUxThemeDll = LoadLibrary(TEXT("uxtheme.dll"));
    if (!g_hUxThemeDll)
        return FALSE;

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif

#if defined(WITH_THEMES)

#if defined(WINE)
    *(FARPROC *)&OpenThemeFile =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(2));
#endif

    *(FARPROC *)&GetThemeDefaults =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(7));

#if defined(WINE)
    *(FARPROC *)&ApplyTheme =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(4));
#else
    *(FARPROC *)&SetSystemVisualStyle =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(65));
#endif

#if WINVER >= WINVER_8 && !defined(WINE)
    *(FARPROC *)&OpenThemeDataFromFile =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(16));

    *(FARPROC *)&LoaderLoadTheme =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(92));
#endif

#endif  /* defined(WITH_THEMES) */

#if WINVER >= WINVER_8 && !defined(WINE)
    *(FARPROC *)&GetUserColorPreference =
        GetProcAddress(g_hUxThemeDll, "GetUserColorPreference");

    *(FARPROC *)&SetUserColorPreference =
        GetProcAddress(g_hUxThemeDll, MAKEINTRESOURCEA(122));
#endif

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

    return TRUE;
}

void UnloadUxThemeFunctions(void)
{
#if defined(WITH_THEMES)

#if defined(WINE)
    OpenThemeFile = NULL;
#endif
    GetThemeDefaults = NULL;
#if defined(WINE)
    ApplyTheme = NULL;
#else
    SetSystemVisualStyle = NULL;
#endif
#if WINVER >= WINVER_8 && !defined(WINE)
    OpenThemeDataFromFile = NULL;
    LoaderLoadTheme = NULL;
#endif

#endif  /* defined(WITH_THEMES) */

#if WINVER >= WINVER_8 && !defined(WINE)
    GetUserColorPreference = NULL;
    SetUserColorPreference = NULL;
#endif

    if (g_hUxThemeDll)
    {
        FreeLibrary(g_hUxThemeDll);
        g_hUxThemeDll = NULL;
    }
}

#if defined(WITH_THEMES)
/* Apply a visual style if it is different to the currently set one.
 * pszStyleFile should be an absolute expanded path.
 * colors is populated with the style default colors.
 * SetSystemVisualStyle() does not work on WOW64.
 */
_Success_(return == S_OK)
HRESULT ApplyVisualStyle(
    _In_z_     const WCHAR *pszStyleFile,
    _In_opt_z_ const WCHAR *pszColor,
    _In_opt_z_ const WCHAR *pszSize
#if WINVER >= WINVER_8 && !defined(WINE)
  , _Inout_updates_(NUM_COLORS) COLORREF *colors
#endif
)
{
#if defined(WINE)
    if (!OpenThemeFile || !ApplyTheme)
#else
    if (!SetSystemVisualStyle)
#endif
        return E_POINTER;

#if WINVER >= WINVER_8 && !defined(WINE)
    if (!OpenThemeDataFromFile || !LoaderLoadTheme)
        return E_POINTER;
#endif

#if WINVER > WINVER_8
    if (!IsValidVisualStylePath(pszStyleFile))
        return E_INVALIDARG;
#endif

    HRESULT hr;
    WCHAR pszRealColor[MAX_PATH];
    WCHAR pszRealSize[MAX_PATH];
    pszRealColor[0] = L'\0';
    pszRealSize[0] = L'\0';

    /* Get default style color and/or size if any of them was not specified */

    WCHAR *pszDefaultColor = NULL;
    INT cchMaxColorChars = 0;
    WCHAR *pszDefaultSize = NULL;
    INT cchMaxSizeChars = 0;

    if (GetThemeDefaults)
    {
        if (!pszColor || pszColor[0] == L'\0')
        {
            pszDefaultColor = pszRealColor;
            cchMaxColorChars = MAX_PATH;
        }

        if (!pszSize || pszSize[0] == L'\0')
        {
            pszDefaultSize = pszRealSize;
            cchMaxSizeChars = MAX_PATH;
        }

        if (pszDefaultColor || pszDefaultSize)
        {
            hr = GetThemeDefaults(pszStyleFile,
                pszDefaultColor, cchMaxColorChars,
                pszDefaultSize, cchMaxSizeChars);
        }
        else
            hr = S_OK;
    }
    else
        hr = E_POINTER;

    if ((hr != S_OK || !pszDefaultColor) &&
        pszColor && pszColor[0] != L'\0')
    {
        lstrcpyW(pszRealColor, pszColor);
    }

    if ((hr != S_OK || !pszDefaultSize) &&
        pszSize && pszSize[0] != L'\0')
    {
        lstrcpyW(pszRealSize, pszSize);
    }

    /* Compare with current values to avoid unnecessary flashing if the
     * style will not change
     */
    if (IsCurrentVisualStyle(pszStyleFile, pszRealColor, pszRealSize))
        return S_OK;

#if defined(WINE)

    HTHEME hTheme = OpenMsStles(pszStyleFile, pszRealColor, pszRealSize);
    if (!hTheme)
        return E_HANDLE;

#elif WINVER >= WINVER_8

    UXTHEMEFILE uxThemeFile;
    HTHEME hTheme = OpenMsStles(pszStyleFile, pszRealColor, pszRealSize,
        &uxThemeFile);
    if (!hTheme)
        return E_HANDLE;

#endif

#if defined(WINE)
    hr = ApplyTheme(hTheme, 1, NULL);
#else
    hr = SetSystemVisualStyle(pszStyleFile, pszRealColor, pszRealSize, 1);
#endif
    if (hr != S_OK)
        return hr;

    /* Message sent by Windows when changing visual styles; not sure if it is
     * necessary.
     */
    SendNotifyMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0L,
#if WINVER >= WINVER_8  /* Also sometimes on 7 when changing colors */
        (LPARAM)TEXT("WindowsThemeElement")
#else
        (LPARAM)TEXT("Windows")
#endif
    );

#if WINVER >= WINVER_8 && !defined(WINE)
    /* Load default msstyles colors.
     * Unneeded on XP and Wine.
     * Not working on Vista/7: GetThemeSysColor() always returns 0.
     */
    for (int iColor = 0; iColor < NUM_COLORS; iColor++)
        colors[iColor] = GetThemeSysColor(hTheme, iColor);

    UnmapViewOfFile(uxThemeFile.sharableSectionView);
    UnmapViewOfFile(uxThemeFile.nonSharableSectionView);
#endif

#if WINVER >= WINVER_8 || defined(WINE)
    CloseThemeData(hTheme);
#endif

    return S_OK;
}
#endif  /* defined(WITH_THEMES) */

#if WINVER >= WINVER_8 && !defined(WINE)
COLORREF GetAccentColor(void)
{
    if (!GetUserColorPreference)
        return CLR_INVALID;

    if (GetUserColorPreference(&g_colors, FALSE) != S_OK)
        return CLR_INVALID;

    return g_colors.color2 & 0x00FFFFFF;
}

_Success_(return == S_OK)
HRESULT SetAccentColor(COLORREF color)
{
    if (!SetUserColorPreference)
        return E_POINTER;

    g_colors.color2 = color;
    return SetUserColorPreference(&g_colors, TRUE);
}
#endif

#endif  /* defined(WITH_THEMES) && WINVER >= WINVER_XP */
