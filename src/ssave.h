#pragma once

#include "config.h"

#if !defined(SSAVE_H) && defined(WITH_THEMES)
#define SSAVE_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

extern TCHAR szScreenSaverValue[];

_Success_(return)
BOOL ApplyScreenSaver(_In_z_ const TCHAR *path);

#endif  /* !defined(SSAVE_H) && defined(WITH_THEMES) */
