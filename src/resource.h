#pragma once
#if !defined(RESOURCE_H)
#define RESOURCE_H

#include "config.h"

#define VERSION      1,5,8,0
#define VERSION_STR  "1.5.8\0"

#define WC_SCHEMEPREVIEW  TEXT("SchemePreviewWndClass")

/* String constants */

#define NEWLINE "\r\n"

/* Internal names for command line usage */
#if defined(WITH_CLASSIC)
#define PAGENAME_CLASSIC  L"Appearance"
#endif
#if defined(WITH_EFF)
#define PAGENAME_EFF      L"Effects"
#endif


/* *** Resource IDs *** */

/* Menus */

#define IDM_PREVIEW_MENU                  100
#define IDM_PREVIEW_NORMAL                101
#define IDM_PREVIEW_DISABLED              102
#define IDM_PREVIEW_SELECTED              103

#if defined(WITH_PREVIEW_CONTEXT_MENU)
#define IDM_PREVIEW_CONTEXT_MENU          110
#define IDM_PREVIEW_CLASSIC               111
#endif


/* Icons */
#define IDI_PROGRAM                       100
#define IDI_SAVEICON                      101
#define IDI_RENAMEICON                    102
#define IDI_DELICON                       103


/* Dialogs */
#if defined(WITH_CLASSIC)
#define IDD_CLASSIC                       300
#define IDD_CLASSIC_SAVE                  301
#define IDD_CLASSIC_RENAME                302
#endif
#if defined(WITH_EFF)
#define IDD_EFF                           600
#define IDD_EFF_ANIM                      601
#endif


/* Controls */

#define IDC_STATIC  -1

#if defined(WITH_CLASSIC)
/* Classic Visual Style page */
#define IDC_CLASSIC_PREVIEW               100
#define IDC_CLASSIC_SCHEME                101
#define IDC_CLASSIC_SCHEMESIZE            102
#define IDC_CLASSIC_SAVE                  103
#define IDC_CLASSIC_RENAME                104
#define IDC_CLASSIC_DELETE                105
#define IDC_CLASSIC_ELEMENT               106
#define IDC_CLASSIC_SIZE1_T               107
#define IDC_CLASSIC_SIZE1_E               108
#define IDC_CLASSIC_SIZE1_UD              109
#define IDC_CLASSIC_SIZE2_T               110
#define IDC_CLASSIC_SIZE2_E               111
#define IDC_CLASSIC_SIZE2_UD              112
#define IDC_CLASSIC_COLOR1_T              113
#define IDC_CLASSIC_COLOR1                114
#define IDC_CLASSIC_COLOR2_T              115
#define IDC_CLASSIC_COLOR2                116
#define IDC_CLASSIC_FONTNAME_T            117
#define IDC_CLASSIC_FONTNAME              118
#define IDC_CLASSIC_FONTSIZE_T            119
#define IDC_CLASSIC_FONTSIZE              120
#define IDC_CLASSIC_FONTWIDTH_T           121
#define IDC_CLASSIC_FONTWIDTH_E           122
#define IDC_CLASSIC_FONTWIDTH_UD          123
#define IDC_CLASSIC_FONTCOLOR_T           124
#define IDC_CLASSIC_FONTCOLOR             125
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define IDC_CLASSIC_FONTANGLE_T           126
#define IDC_CLASSIC_FONTANGLE_E           127
#define IDC_CLASSIC_FONTANGLE_UD          128
#endif
#define IDC_CLASSIC_FONTSTYLE_T           129
#define IDC_CLASSIC_FONTBOLD              130
#define IDC_CLASSIC_FONTITALIC            131
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define IDC_CLASSIC_FONTUNDERLINE         132
#endif
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS) && WINVER >= WINVER_XP
#define IDC_CLASSIC_FONTSMOOTHING_T       133
#define IDC_CLASSIC_FONTSMOOTHING         134
#endif
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS) && WINVER >= WINVER_2K
#define IDC_CLASSIC_GRADIENTCAPTIONS      135
#endif
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS) && WINVER >= WINVER_XP
#define IDC_CLASSIC_FLATMENUS             136
#endif

#define IDC_CLASSIC_NEWNAME               100
#define IDC_CLASSIC_NEWSIZE               101
#endif  /* defined(WITH_CLASSIC) */

#if defined(WITH_EFF)
/* Effects page */
#define IDC_EFF_ICONS_SIZE_E              100
#define IDC_EFF_ICONS_SIZE_UD             101
#define IDC_EFF_ICONS_XMARGIN_E           102
#define IDC_EFF_ICONS_XMARGIN_UD          103
#define IDC_EFF_ICONS_YMARGIN_E           104
#define IDC_EFF_ICONS_YMARGIN_UD          105
#if WINVER >= WINVER_XP
#define IDC_EFF_ICONS_LABELSHADOW         106
#endif
#if WINVER >= WINVER_VISTA
#define IDC_EFF_ICONS_TRANSSELRECT        107
#define IDC_EFF_ICONS_CUSTOMIZE           108
#endif
#if WINVER >= WINVER_2K
#define IDC_EFF_MASTER                    109
#endif
#define IDC_EFF_ANIMATIONS                110
#if WINVER >= WINVER_XP
#define IDC_EFF_MENUSHADOW                111
#define IDC_EFF_POINTERSHADOW             112
#endif
#if WINVER >= WINVER_2K
#define IDC_EFF_DRAGFULLWIN               113
#endif
#if WINVER >= WINVER_2K && !defined(WINVER_IS_98)
#define IDC_EFF_FONTSMOOTHING             114
#endif
#if WINVER >= WINVER_XP
#define IDC_EFF_FONTSMOOTHING_TYPE        115
#endif
#if WINVER >= WINVER_2K
#define IDC_EFF_KEYBOARDCUES              116
#if !defined(WINVER_IS_98)
#define IDC_EFF_RIGHTALIGNMENU            117
#endif
#define IDC_EFF_HIGHCONTRAST              118
#endif  /* WINVER >= WINVER_2K */
#define IDC_EFF_RESET                     119

#define IDC_ANIM_MENUOPEN                 100
#define IDC_ANIM_MENUOPEN_TYPE            101
#define IDC_ANIM_MENUSEL                  102
#define IDC_ANIM_TOOLTIP                  103
#define IDC_ANIM_TOOLTIP_TYPE             104
#define IDC_ANIM_COMBOBOX                 105
#define IDC_ANIM_LISTBOX                  106
#define IDC_ANIM_WINDOW                   107
#if WINVER >= WINVER_VISTA
#define IDC_ANIM_CONTROL                  108
#endif
#endif  /* defined(WITH_EFF) */


/* Strings */

#define IDS_PROPSHEET_NAME                 41

#if WINVER >= WINVER_2K
/* For the Control Panel icon */
#define IDS_APP_DESCRIPTION                42
#define IDS_APP_NAME                      100
#endif

#define IDS_ERROR                         101
#define IDS_ERROR_GENERIC                 102
#define IDS_ERROR_MEM                     103

#if defined(WITH_CMDLINE)
#define IDS_USAGE_GENERAL                 104
#endif

#if defined(WITH_THEMES)
#define IDS_ERROR_UXTHEME                 110
#define IDS_ERROR_THEME                   111
#define IDS_ERROR_MSSPATH                 112
#endif

#if defined(WITH_CLASSIC)
#define IDS_CURRENT_ELEMENT               120
#endif

#if defined(WITH_THEMES)
#if defined(WITH_CMDLINE)
#define IDS_FILEPATH_INVALID              140
#endif
#endif

#if defined(WITH_CLASSIC)

/* Preview window */
#define IDS_INACTIVE_CAPTION              200
#define IDS_ACTIVE_CAPTION                201
#define IDS_WINDOW_TEXT                   202
#define IDS_MESSAGE_CAPTION               203
#define IDS_MESSAGE_TEXT                  204
#define IDS_OK                            205
#define IDS_MAIN_CAPTION                  206
#define IDS_DISABLED_TEXT                 207
#define IDS_NORMAL_TEXT                   208
#define IDS_SELECTED_TEXT                 209
#define IDS_PALETTE_CAPTION               210
#define IDS_BUTTON_TEXT                   211
#define IDS_TOOLTIP_TEXT                  212
#define IDS_ICON_LABEL                    213

/* Classic Visual Style elements */
/* Note: Keep in sync with wndclas.h */
#define IDS_ELEMENT_0                     300
#define IDS_ELEMENT_1                     301
#define IDS_ELEMENT_2                     302
#define IDS_ELEMENT_3                     303
#define IDS_ELEMENT_4                     304
#define IDS_ELEMENT_5                     305
#define IDS_ELEMENT_6                     306
#define IDS_ELEMENT_7                     307
#define IDS_ELEMENT_8                     308
#define IDS_ELEMENT_9                     309
#define IDS_ELEMENT_10                    310
#define IDS_ELEMENT_11                    311
#define IDS_ELEMENT_12                    312
#define IDS_ELEMENT_13                    313
#define IDS_ELEMENT_14                    314
#define IDS_ELEMENT_15                    315
#define IDS_ELEMENT_16                    316
#if WINVER >= WINVER_2K
#define IDS_ELEMENT_17                    317
#define IDS_HYPERLINK_TEXT     IDS_ELEMENT_17
#endif
#if WINVER >= WINVER_XP
#define IDS_ELEMENT_18                    318
#endif
#if WINVER >= WINVER_VISTA
#define IDS_ELEMENT_19                    319
#endif

#define IDS_BOLD                          330
#define IDS_ITALIC                        331
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define IDS_UNDERLINE                     332
#endif
#define IDS_SAVESCHEME                    333
#define IDS_RENAMESCHEME                  334
#define IDS_DELETESCHEME                  335

#endif  /* defined(WITH_CLASSIC) */

#if (defined(WITH_CLASSIC_ADVANCED_SETTINGS) || defined(WITH_EFF)) && \
    WINVER >= WINVER_XP
#define IDS_FONTSMOOTHING_DEFAULT         336
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define IDS_FONTSMOOTHING_OFF             337
#endif
#define IDS_FONTSMOOTHING_CT              338
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define IDS_FONTSMOOTHING_CTN             339
#endif
#endif

#if defined(WITH_CLASSIC)

/* Save/Rename/Delete Scheme dialogs */
#define IDS_SAVESCHEME_ERROR              350
#define IDS_RENAMESCHEME_ERROR            351
#define IDS_RENAMESIZE_ERROR              352
#define IDS_SAVESCHEME_CONFIRM_OVERWRITE  353
#define IDS_RENAMESCHEME_CONFLICT         354
#define IDS_RENAMESIZE_CONFLICT           355
#define IDS_SAVESCHEME_NO_NAME            356
#define IDS_SAVESCHEME_NO_SIZE            357
#define IDS_SAVESCHEME_DEFAULT_NAME       358
#define IDS_SAVESCHEME_DEFAULT_SIZE       359
#define IDS_DELETE_CONFIRM_TITLE          360
#define IDS_DELETESCHEME_CONFIRM_SCHEME   361
#define IDS_DELETESCHEME_CONFIRM_SIZE     362
#define IDS_DELETESCHEME_ERROR_SCHEME     363
#define IDS_DELETESCHEME_ERROR_SIZE       364

#if defined(WITH_CMDLINE)
#define IDS_USAGE_APPEARANCE              370
#define IDS_SCHEME                        371
#define IDS_SIZES                         372
#define IDS_SCHEME_LOAD_ERROR             373
#define IDS_NO_SCHEMES                    374
#endif

#endif  /* defined(WITH_CLASSIC) */

/* Effects dialogs */
#if defined(WITH_EFF) && WINVER >= WINVER_2K

#define IDS_ANIM_SLIDE                    600
#define IDS_ANIM_FADE                     601
#if WINVER >= WINVER_XP
#define IDS_EFF_MENUSHADOW_TT             602
#endif
#define IDS_EFF_KEYBOARDCUES_TT           603
#if !defined(WINVER_IS_98)
#define IDS_EFF_RIGHTALIGNMENU_TT         604
#endif
#define IDS_EFF_HIGHCONTRAST_TT           605
#if WINVER >= WINVER_VISTA
#define IDS_ANIM_TYPE_TT                  606
#endif

#if defined(WITH_CMDLINE)
#define IDS_USAGE_EFF                     650
#if WINVER >= WINVER_XP
#define IDS_HIGHCONTRAST_ERROR            651
#define IDS_HIGHCONTRAST_ENABLED          652
#define IDS_HIGHCONTRAST_DISABLED         653
#endif
#endif  /* defined(WITH_CMDLINE) */

#endif  /* defined(WITH_EFF) && WINVER >= WINVER_2K */

#if WINVER >= WINVER_VISTA
/* For the Control Panel tasks list */
#if defined(WITH_CLASSIC)
#define IDS_CLASSIC                      1243
#define IDS_CLASSIC_KEYWORDS             1253
#endif
#if defined(WITH_EFF)
#define IDS_EFF                          1245
#define IDS_EFF_KEYWORDS                 1255
#endif
#endif  /* WINVER >= WINVER_VISTA */

#endif  /* !defined(RESOURCE_H) */
