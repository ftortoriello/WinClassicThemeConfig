#pragma once

#include "config.h"

#if !defined(THM_H) && defined(WITH_THEMES)
#define THM_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

_Success_(return)
BOOL ApplyTheme(_In_z_ const TCHAR *filePath);

void ShowApplyThemeStyleError(HRESULT hr);

#endif  /* !defined(THM_H) && defined(WITH_THEMES) */
