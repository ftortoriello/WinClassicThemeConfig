#pragma once

#include "config.h"

#if !defined(DWMUNDOC_H) && \
    defined(WITH_THEMES) && WINVER >= WINVER_VISTA && !defined(WINE)
#define DWMUNDOC_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/* Convert intensity percentage (0 - 100) to the most significant byte in a DWM
 * color (0x1A - 0xD9).
 * The intensity value in the color is saved but ignored on 7 and later.
 */
#define Intensity2Dwm(intensity) \
    ((0x1A + intensity * (0xD9 - 0x1A) / 100) << 24)

#define DwmColor2Intensity(color) \
    ((color >> 24) - 0x1A) * 100 / (0xD9 - 0x1A)

#define BGR2RGB(c) RGB(GetBValue(c), GetGValue(c), GetRValue(c))

/* Convert COLORREF (0x00BBGGRR) to DWM color (0xAARRGGBB).
 */
#define RGB2DWM(color, intensity) \
    Intensity2Dwm(intensity) | BGR2RGB(color)

typedef struct tagDWMCOLORPARAMS
{
    /* These values are saved to the registry to:
     * HKCU\Software\Microsoft\Windows\DWM
     */

     /* ColorizationColor: Main DWM color. */
    DWORD crColorization;

#if WINVER >= WINVER_7
    /* ColorizationAfterglow - no effect on 8+ */
    DWORD crAfterGlow;
#endif

    /* ColorizationColorBalance (0 - 120): Transparency level.
     * On 8+: same effect as higher luminescence.
     * The DWM engine allows values up to 120, but in some cases exceeding 100
     * or more causes a completely wrong color to be used with transparency
     * disabled.
     */
    UINT nColorBalance;

#if WINVER >= WINVER_7
    /* ColorizationAfterglowBalance (0 - 120) - no effect on 8+ */
    UINT nAfterglowBalance;

    /* ColorizationBlurBalance (0 - 120) - no effect on 8+ */
    UINT nBlurBalance;

    /* ColorizationGlassReflectionIntensity (0 - 120) */
    UINT nGlassReflectionIntensity;
#endif

    /* ColorizationOpaqueBlend: Disable transparency - Unsupported on 8+ */
    BOOL fOpaqueBlend;
} DWMCOLORPARAMS;

_Success_(return)
BOOL LoadDwmFunctions(void);

void UnloadDwmFunctions(void);

_Success_(return == S_OK)
HRESULT GetDwmColorization(_Out_ DWMCOLORPARAMS *pParams);

_Success_(return == S_OK)
HRESULT SetDwmColorization(_In_ DWMCOLORPARAMS *pParams, BOOL fDontSave);

void GetDwmColorizationDefaults(_Out_ DWMCOLORPARAMS *pParams);

_Success_(return)
BOOL EnableComposition(BOOL bEnable);

#endif  /* !defined(DWMUNDOC_H) &&
         * defined(WITH_THEMES) && WINVER >= WINVER_VISTA && !defined(WINE) */
