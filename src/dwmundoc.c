/*
 * COPYRIGHT: See COPYING in the top level directory
 * PURPOSE:   Destop Window Manager undocumented function wrappers and utils
 *
 * PROGRAMMER: Franco Tortoriello (torto09@gmail.com)
 */

#include "app.h"
#include "dwmundoc.h"

#if defined(WITH_THEMES) && WINVER >= WINVER_VISTA && !defined(WINE)

#if WINVER == WINVER_VISTA
#include <dwmapi.h>
#endif

#if WINVER >= WINVER_7

static
HRESULT(WINAPI *DwmGetColorizationParameters)
    (_Out_ DWMCOLORPARAMS *pParams) = NULL;

static
HRESULT(WINAPI *DwmSetColorizationParameters)
    (_In_ DWMCOLORPARAMS *pParams, BOOL fDontSave) = NULL;

#else

static
HRESULT(WINAPI *DwmSetColorizationColor)
    (DWORD crColorization, BOOL fOpaqueBlend, BOOL fDontSave) = NULL;

#endif

static
DWORD(WINAPI *DwmpRestartComposition)
    (void) = NULL;

static HMODULE g_hDwmDll = NULL;

_Success_(return)
BOOL LoadDwmFunctions(void)
{
    g_hDwmDll = LoadLibrary(TEXT("dwmapi.dll"));
    if (!g_hDwmDll)
        return FALSE;

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif

#if WINVER >= WINVER_7

    *(FARPROC *)&DwmGetColorizationParameters =
        GetProcAddress(g_hDwmDll, MAKEINTRESOURCEA(127));

    *(FARPROC *)&DwmSetColorizationParameters =
        GetProcAddress(g_hDwmDll, MAKEINTRESOURCEA(131));

#else  /* WINVER == WINVER_VISTA */

    *(FARPROC *)&DwmSetColorizationColor =
        GetProcAddress(g_hDwmDll, MAKEINTRESOURCEA(104));

#endif

    *(FARPROC *)&DwmpRestartComposition =
        GetProcAddress(g_hDwmDll, MAKEINTRESOURCEA(103));

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

    return TRUE;
}

void UnloadDwmFunctions(void)
{
#if WINVER >= WINVER_7
    DwmGetColorizationParameters = NULL;
    DwmSetColorizationParameters = NULL;
#else
    DwmSetColorizationColor = NULL;
#endif
    DwmpRestartComposition = NULL;

    FreeLibrary(g_hDwmDll);
    g_hDwmDll = NULL;
}

_Success_(return == S_OK)
HRESULT GetDwmColorization(_Out_ DWMCOLORPARAMS *pParams)
{
#if WINVER >= WINVER_7

    if (!DwmGetColorizationParameters)
        return E_POINTER;

    return DwmGetColorizationParameters(pParams);

#else  /* WINVER == WINVER_VISTA */

    HRESULT hr = DwmGetColorizationColor(
        &pParams->crColorization,
        &pParams->fOpaqueBlend);

    if (hr == S_OK)
        pParams->nColorBalance = DwmColor2Intensity(pParams->crColorization);

    return hr;

#endif
}

/* On Vista, crColorization must already include the correct balance value */
_Success_(return == S_OK)
HRESULT SetDwmColorization(_In_ DWMCOLORPARAMS *pParams, BOOL fDontSave)
{
#if WINVER >= WINVER_7

    if (!DwmSetColorizationParameters)
        return E_POINTER;

    return DwmSetColorizationParameters(pParams, fDontSave);

#else  /* WINVER == WINVER_VISTA */

    if (!DwmSetColorizationColor)
        return E_POINTER;

    return DwmSetColorizationColor(
        pParams->crColorization,
        pParams->fOpaqueBlend,
        fDontSave);

#endif
}

void GetDwmColorizationDefaults(_Out_ DWMCOLORPARAMS *pParams)
{
#if WINVER >= WINVER_8
    pParams->crColorization = 0xC40078D4;  /* 11; on 10: 0xC40078D7 */
    pParams->crAfterGlow = pParams->crColorization;
    pParams->nColorBalance = 89;
    pParams->nAfterglowBalance = 10;
    pParams->nBlurBalance = 1;
    pParams->nGlassReflectionIntensity = 0;  /* 11; on 8 and 10: 1 */
    pParams->fOpaqueBlend = TRUE;
#elif WINVER >= WINVER_7
    pParams->crColorization = 0x6B74B8FC;
    pParams->crAfterGlow = pParams->crColorization;
    pParams->nColorBalance = 8;
    pParams->nAfterglowBalance = 43;
    pParams->nBlurBalance = 49;
    pParams->nGlassReflectionIntensity = 50;
    pParams->fOpaqueBlend = FALSE;
#else  /* WINVER == WINVER_VISTA */
    pParams->crColorization = 0x45409EFE;
    pParams->nColorBalance = 22;
    pParams->fOpaqueBlend = FALSE;
#endif
}

/* The DwmEnableComposition() functions are only meant for temporarily disabling
 * the compositor for compatibility; it is only kept disabled while the process
 * runs.
 * This disables or enables it permanently on Vista and 7.
 */
_Success_(return)
BOOL EnableComposition(BOOL bEnable)
{
    if (!DwmpRestartComposition)
        return FALSE;

    HKEY hKey;
    LSTATUS status = RegCreateKeyEx(HKEY_CURRENT_USER,
        TEXT("Software\\Microsoft\\Windows\\DWM"),
        0, NULL, 0, KEY_SET_VALUE, NULL, &hKey, NULL);
    if (status != ERROR_SUCCESS)
        return FALSE;

    DWORD dwData = (bEnable ? 1 : 0);
    status = RegSetValueEx(hKey, TEXT("Composition"), 0, REG_DWORD,
        (BYTE *)&dwData, sizeof(DWORD));

    RegCloseKey(hKey);

    if (status != ERROR_SUCCESS)
        return FALSE;

    return DwmpRestartComposition() == 0;
}

#endif  /* defined(WITH_THEMES) && WINVER >= WINVER_VISTA && !defined(WINE) */
