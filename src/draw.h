#pragma once

#include "config.h"

#if !defined(DRAW_H) && defined(WITH_CLASSIC)
#define DRAW_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

_Success_(return)
BOOL DrawBorder(HDC hdc, _In_ const RECT *pRect, int borderWidth,
    HBRUSH hBrush);

int GetCaptionIconSize(int availableSpace, _Out_ int *pTotalMargin);

#endif  /* !defined(DRAW_H) && defined(WITH_CLASSIC) */
