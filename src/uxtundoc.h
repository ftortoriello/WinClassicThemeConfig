#pragma once

#include "config.h"

#if !defined(UXTUNDOC_H) && defined(WITH_THEMES) && WINVER >= WINVER_XP
#define UXTUNDOC_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <uxtheme.h>

#include "classtyl.h"

_Success_(return)
BOOL LoadUxThemeFunctions(void);

void UnloadUxThemeFunctions(void);

#if defined(WITH_THEMES)
_Success_(return == S_OK)
HRESULT ApplyVisualStyle(
    _In_z_     const WCHAR *pszStyleFile,
    _In_opt_z_ const WCHAR *pszColor,
    _In_opt_z_ const WCHAR *pszSize
#if WINVER >= WINVER_8 && !defined(WINE)
  , _Inout_updates_(NUM_COLORS) COLORREF *colors
#endif
);
#endif

#if WINVER >= WINVER_8 && !defined(WINE)
COLORREF GetAccentColor(void);

_Success_(return == S_OK)
HRESULT SetAccentColor(COLORREF color);
#endif

#endif  /* !defined(UXTUNDOC_H) &&
         * defined(WITH_THEMES) && WINVER >= WINVER_XP */
