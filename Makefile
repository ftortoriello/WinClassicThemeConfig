.POSIX:
.SUFFIXES:
.SUFFIXES: .obj .c .res .rc

PROG = Themes.exe

CC = cc
RM = rm -vf
WINDRES = windres
STRIP = strip

MY_CFLAGS = -Wall -Wextra -Wpedantic $(CFLAGS)
MY_CPPFLAGS = -D_WINDOWS $(CPPFLAGS)

LDFLAGS ?= -Wl,-subsystem,windows -lshlwapi -luxtheme -lole32 -luuid
MY_LDFLAGS = -s -nostdlib -Wl,-e__main -Wl,--enable-stdcall-fixup\
	-ladvapi32 -lcomctl32 -lcomdlg32 -lgdi32 -lkernel32 -lshell32 -luser32\
	$(LDFLAGS)

OBJ =\
	src/mincrt.obj\
	src/reg.obj\
	src/util.obj\
	src/classtyl.obj\
	src/dwmundoc.obj\
	src/uxtundoc.obj\
	src/bg.obj\
	src/ssave.obj\
	src/thmsnd.obj\
	src/thm.obj\
	src/eff.obj\
	src/colorbtn.obj\
	src/draw.obj\
	src/clasdraw.obj\
	src/preview.obj\
	src/dlgcsave.obj\
	src/dlgcren.obj\
	src/wndclas.obj\
	src/dlganim.obj\
	src/wndeff.obj\
	src/cmdthm.obj\
	src/cmdclas.obj\
	src/cmdeff.obj\
	src/main.obj\

RES = res/app.res

all: $(PROG)

$(PROG): $(OBJ) $(RES)
	$(CC) -o $@ $(OBJ) $(RES) $(MY_LDFLAGS)
	$(STRIP) $@

.c.obj:
	$(CC) -c $(MY_CPPFLAGS) $(MY_CFLAGS) -o $@ $<

.rc.res:
	$(WINDRES) $(MY_CPPFLAGS) $< -O coff -I. -o $@

clean: cleanres
	$(RM) $(PROG)
	$(RM) $(OBJ)

cleanres:
	$(RM) $(RES)
