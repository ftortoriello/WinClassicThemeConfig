# Windows Classic Appearance Configurator

[Thread on WinClassic](https://winclassic.net/thread/1436/new-classic-theme-configurator)

## Advantages from official Windows releases

* Works on all Windows versions starting from 95 and NT4.
  This makes it possible to configure the classic theme ("unskinned" windows) on
  modern versions, where support has been removed.
  It also works on Wine and WinPE!
  You can even import schemes and colors from 16-bit versions.
  Not all versions support all features.

* Allows configuring advanced settings which are hidden on the system applet.

* Allows saving and deleting schemes and sizes.

* Improved preview. If you prefer the classic layout, check the "Classic
  preview window" item in the preview window context menu.

* Allows configuring other aspects of the OS which were either hidden or
  configurable only from modern Apps.

* Allows applying .Theme files, including additional features and features
  removed from newer system versions, such as colors, metrics and desktop
  patterns.
  You can also apply a .msstyles file directly.

* Command line support; run with `/?` to see how to use it.
  Requires XP or later.

## Warnings

* Use the executable for your OS version and architecture, if available, or for
  the closest previous version.
  For instance, you will not be able to change the modern visual style on
  a 64-bit Windows 10 with the x86 build.

* Some Windows versions are less tested than others, so there may be bugs.
  Please, let me know if you find any issues.

* Be careful when changing modern styles and metrics.
  The OS may become unbootable under certain specific conditions, so you should
  always have a backup and know how to recover the system.

## Notes

* The purpose of this tool is not to enable using the classic theme on modern
  Windows versions, only to configure it.

* The classic schemes are stored in the registry, in
  `HKCU\Control Panel\Appearance\ClassicSchemes`. On the first launch, system
  schemes are imported.

* This software has _no_ external dependencies, such as the Visual C runtime.
  The only exception is on 95 an NT4, where it requires `comctl32.dll` 5.80 or
  greater (you can get it from `50comupd`, the Visual C 6.0 redistributable, or
  IE5).

* Parts of the source code were initially based on the `desk.cpl` implementation
  from [ReactOS](https://reactos.org).

* This software is licensed under the GPL version 2 or later.
  Feel free to fork the code to improve it! Just make sure to respect the
  license and mention the original authors.

## Building the program

* A standard Makefile and scripts are provided for MinGW.

  * Running `mingw32-make` from a MinGW environment builds an executable for
    the latest supported version.
    You can set the target OS by running, for instance,
    `mingw32-make CPPFLAGS='-DWINVER=0x0500 -DUNICODE'` to target Windows 2000.

  * To build for old PCs running Windows 9x, you may need to lower the CPU
    target, otherwise the program will crash on launch.
    For instance, to target Windows 95, use
    `mingw32-make CPPFLAGS='-DWINVER=0x0400' CFLAGS='-O2 -march=pentium'`.

  * The `makeall` shell scripts build versions for supported targets.

  * Example for cross-compilation from GNU/Linux, by specifying the cc, windres
    and strip tools, an optimization level, and the number of parallel jobs:
    `make CC=x86_64-w64-mingw32-gcc WINDRES=x86_64-w64-mingw32-windres
    STRIP=x86_64-w64-mingw32-strip CFLAGS=-O2 -j5`

* In addition, on the `vs17` folder there is the `Themes.sln` solution for
  Microsoft Visual Studio 2022. There are configurations for different Windows
  versions and architectures.

  * To build using Visual Studio for versions older than XP, the PE OS version
    fields need to be edited. The build process requires placing
    `EditVersion.exe` from [LegacyExtender](http://www.legacyextender.com) on
    the root directory (`Themes`).

  * To build for old PCs running Windows 9x, you may need to disable
    optimizations (as configured in the solution).

### Preprocessor definitions

* `WINVER`: Determines the target Windows version.
  You can see the sopported versions and its values on `src/winver.h`.
  Defaults to Windows 10 / 11 (0x0A00).

* `UNICODE`: Use the Unicode instead of multibyte variant of the Windows API
  functions.
  It should be defined for NT versions, and left undefined for 9x.
  For instance, to target XP, define `WINVER=0x0501` and `UNICODE`.
  Defaults to defined for XP and later.

* `WINE`: Workarounds some bugs when running on Wine

* See `src/config.h` for other build settings.
