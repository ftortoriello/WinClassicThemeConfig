#define RES_LANG     LANG_ENGLISH
#define RES_SUBLANG  SUBLANG_ENGLISH_US


/* Menus */

#define S_PREVIEW_MENU_NORMAL    "&Normal"
#define S_PREVIEW_MENU_DISABLED  "&Disabled"
#define S_PREVIEW_MENU_SELECTED  "&Selected"

#define S_PREVIEW_MENU_CLASSIC  "&Classic preview window"


/* Dialog strings */

#define S_OK      "OK"
#define S_CANCEL  "Cancel"

#define S_CLASSIC                   "Classic Style"
#define S_CLASSIC_SCHEME            "&Color scheme:"
#define S_CLASSIC_SCHEMESIZE        "Si&ze:"
#define S_CLASSIC_ELEMENT           "&Item:"
#define S_CLASSIC_SIZE1             "&Size 1:"
#define S_CLASSIC_SIZE2             "Size 2:"
#define S_CLASSIC_COLOR1            "Color &1:"
#define S_CLASSIC_COLOR2            "Color &2:"
#define S_CLASSIC_FONTNAME          "&Font:"
#define S_CLASSIC_FONTSIZE          "Size:"
#define S_CLASSIC_FONTWIDTH         "Width:"
#define S_CLASSIC_FONTCOLOR         "Colo&r:"
#define S_CLASSIC_FONTANGLE         "Angle:"
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define S_CLASSIC_FONTSTYLE         "Font style:"
#else
#define S_CLASSIC_FONTSTYLE         "Style:"
#endif
#define S_CLASSIC_FONTBOLD          "B"
#define S_CLASSIC_FONTITALIC        "I "
#define S_CLASSIC_FONTUNDERLINE     "U"
#define S_CLASSIC_FONTSMOOTHING     "Edge smoothing:"
#define S_CLASSIC_GRADIENTCAPTIONS  "Title bar &gradients"
#define S_CLASSIC_FLATMENUS         "Flat &menus"

#define S_SAVESCHEME    "Save Scheme"
#define S_RENAMESCHEME  "Rename Scheme"
#define S_DELETESCHEME  "Delete Scheme"
#define S_BOLD          "Bold"
#define S_ITALIC        "Italic"
#define S_UNDERLINE     "Underline"

#define S_EFF                     "Effects"
#define S_EFF_ICONS               "Desktop icons"
#define S_EFF_ICONS_SIZE          "Size:"
#define S_EFF_ICONS_XMARGIN       "Horizontal margin:"
#define S_EFF_ICONS_YMARGIN       "Vertical margin:"
#define S_EFF_ICONS_LABELSHADOW   "Label shadows"
#define S_EFF_ICONS_TRANSSELRECT  "Translucent selection rectangle"
#define S_EFF_ICONS_CUSTOMIZE     "Customize &icons..."
#define S_EFF_EFF                 "Visual effects"
#define S_EFF_MASTER              "Enable visual effects"
#define S_EFF_ANIMATIONS_BUTTON   "&Animations..."
#define S_EFF_MENUSHADOW          "Menu shadows"
#define S_EFF_MENUSHADOW_TT       "If the window compositor (DWM) and a modern style are enabled, this also enables window shadows."
#define S_EFF_POINTERSHADOW       "Mouse pointer shadow"
#define S_EFF_DRAGFULLWIN         "Show window contents while dragging"
#if WINVER >= WINVER_XP
#define S_EFF_FONTSMOOTHING       "Font edge smoothing:"
#else
#define S_EFF_FONTSMOOTHING       "Font edge smoothing"
#endif
#define S_EFF_APPEARANCE          "Appearance"
#define S_EFF_KEYBOARDCUES        "Always &underline keyboard shortcuts (Alt+key)"
#define S_EFF_KEYBOARDCUES_TT     "If unset, menu keyboard shortcuts and access keys are only underlined after pressing the Alt key."
#define S_EFF_RIGHTALIGNMENU      "Right-align menus"
#define S_EFF_RIGHTALIGNMENU_TT   "Show submenus and pop-up menus to the left, if possible."
#define S_EFF_HIGHCONTRAST        "High &Contrast mode"
#define S_EFF_HIGHCONTRAST_TT     "Hint programs to use a high contrast mode. This does not affect the system colors."
#define S_EFF_RESET               "&Restore defaults"

#define S_ANIM           "Animations"
#define S_ANIM_MENUOPEN  "Menu opening:"
#define S_ANIM_MENUSEL   "Menu item selection (fade)"
#define S_ANIM_TOOLTIP   "ToolTip:"
#define S_ANIM_COMBOBOX  "Combo box opening (slide)"
#define S_ANIM_LISTBOX   "List box smooth scrolling"
#define S_ANIM_WINDOW    "Window minimizing, maximizing and restoring"
#define S_ANIM_CONTROL   "Controls and elements inside windows"
#define S_ANIM_FADE      "Fade"
#define S_ANIM_SLIDE     "Slide"
#define S_ANIM_TYPE_TT   "Note that if the window compositor (DWM) is enabled, slide animations may have a white background."


/* String Tables */

#define S_PROPSHEET_NAME    "Appearance"
#define S_APP_DESCRIPTION   "Change the appearance of your desktop, including colors, fonts and sizes."
#define S_APP_NAME          "Classic Appearance Configurator"
#define S_CLASSIC_KEYWORDS  "classic style;classic theme;color schemes;fonts;sizes"
#define S_EFF_KEYWORDS      "visual effects;high contrast;performance"

#define S_ERROR          "Error"
#define S_ERROR_GENERIC  "An error has occurred."
#define S_ERROR_MEM      "Could not allocate memory."
#define S_ERROR_UXTHEME  "Could not load UxTheme functions."
#define S_ERROR_THEME    "Could not apply the theme."
#define S_ERROR_MSSPATH  "Cannot apply a style file in a user folder."

#define S_CURRENT_ELEMENT  "(Current)"

#define S_INACTIVE_CAPTION  "Inactive Window"
#define S_ACTIVE_CAPTION    "Active Window"
#define S_WINDOW_TEXT       "Window Text"
#define S_MESSAGE_CAPTION   "Message Box"
#define S_MESSAGE_TEXT      "Message Text"
#define S_MAIN_CAPTION      "Preview"
#define S_DISABLED_TEXT     "Disabled Text"
#define S_NORMAL_TEXT       "Normal Text"
#define S_SELECTED_TEXT     "Selected Text"
#define S_PALETTE_CAPTION   "Palette Window"
#define S_BUTTON_TEXT       "Button"
#define S_TOOLTIP_TEXT      "ToolTip."
#define S_ICON_LABEL        "Icon"

#define S_ELEMENT_0   "Desktop"
#define S_ELEMENT_1   "Application Background"
#define S_ELEMENT_2   "Window"
#define S_ELEMENT_3   "Menu"
#define S_ELEMENT_4   "Title Bar - Active"
#define S_ELEMENT_5   "Title Bar - Inactive"
#define S_ELEMENT_6   "Title Bar - Palette"
#define S_ELEMENT_7   "Window Border - Active"
#define S_ELEMENT_8   "Window Border - Inactive"
#define S_ELEMENT_9   "Scrollbar"
#define S_ELEMENT_10  "3D Object"
#define S_ELEMENT_11  "3D Shadow"
#define S_ELEMENT_12  "3D Light"
#define S_ELEMENT_13  "Selected Item"
#define S_ELEMENT_14  "Disabled Item"
#define S_ELEMENT_15  "ToolTip"
#define S_ELEMENT_16  "Message Box"  /* (not Dialog Box) */
#define S_ELEMENT_17  "Hyperlink"
#define S_ELEMENT_18  "Menu Bar (Flat)"
#define S_ELEMENT_19  "Window Padded Border"

#define S_FONTSMOOTHING_DEFAULT  "Default"
#define S_FONTSMOOTHING_OFF      "Disabled"
#define S_FONTSMOOTHING_CT       "ClearType"
#define S_FONTSMOOTHING_CTN      "Natural ClearType"

#define S_SAVESCHEME_ERROR              "The scheme could not be saved."
#define S_RENAMESCHEME_ERROR            "The scheme could not be renamed."
#define S_RENAMESIZE_ERROR              "The scheme size could not be renamed."
#define S_SAVESCHEME_CONFIRM_OVERWRITE  "There is already a scheme and size with those names." \
                                        NEWLINE "Overwrite it?"
#define S_RENAMESCHEME_CONFLICT         "There is already a scheme with that name."
#define S_RENAMESIZE_CONFLICT           "There is already a scheme size with that name."
#define S_SAVESCHEME_NO_NAME            "You must specify a scheme name."
#define S_SAVESCHEME_NO_SIZE            "You must specify a size name."
#define S_SAVESCHEME_DEFAULT_NAME       "Custom Scheme"
#define S_SAVESCHEME_DEFAULT_SIZE       "Normal"
#define S_DELETE_CONFIRM_TITLE          "Deletion Confirmation"
#define S_DELETESCHEME_CONFIRM_SCHEME   "Are you sure you want to delete the selected scheme?"
#define S_DELETESCHEME_CONFIRM_SIZE     "Are you sure you want to delete the selected scheme size?"
#define S_DELETESCHEME_ERROR_SCHEME     "An error has ocurred when trying to delete the scheme."
#define S_DELETESCHEME_ERROR_SIZE       "An error has ocurred when trying to delete the scheme size."

/* Command line usage help */

#if defined(WITH_THEMES)
#define S_USAGE_GENERAL_THEMES \
"file_path" NEWLINE \
"  Apply the specified Theme or MSStyles file." NEWLINE \
NEWLINE
#else
#define S_USAGE_GENERAL_THEMES ""
#endif

/* Do not translate these; they are the command line argument names */
#if defined(WITH_CLASSIC)
#define S_USAGE_PAGES_CLASSIC "  Appearance:  " S_CLASSIC NEWLINE
#else
#define S_USAGE_PAGES_CLASSIC ""
#endif
#if defined(WITH_EFF)
#define S_USAGE_PAGES_EFF     "  Effects:     " S_EFF NEWLINE
#else
#define S_USAGE_PAGES_EFF     ""
#endif

#define S_USAGE_GENERAL \
"Usage:" NEWLINE \
S_USAGE_GENERAL_THEMES \
"[@Page [/Action [Parameters]]]" NEWLINE \
"  For instance: @Appearance /Apply ""Windows Standard"" 1-Normal" NEWLINE \
"  Specify a page without additional parameters to display it selected." NEWLINE \
"  Specify a page with /? to display its command line usage." NEWLINE \
NEWLINE \
"Pages:" NEWLINE \
S_USAGE_PAGES_CLASSIC \
S_USAGE_PAGES_EFF

#define S_USAGE_APPEARANCE  S_CLASSIC " actions:" NEWLINE \
NEWLINE \
"  List" NEWLINE \
"    Display on the console a list of all color schemes and sizes." NEWLINE \
NEWLINE \
"  Apply scheme_name scheme_size" NEWLINE \
"    Apply the specified classic style scheme." NEWLINE \
NEWLINE \
"  SaveCurrent scheme_name scheme_size" NEWLINE \
"    Save the current system scheme with the specified names." NEWLINE \
NEWLINE \
"  ImportWin3 control.ini_path" NEWLINE \
"    Import to the system all color schemes in a Windows 3 CONTROL.INI file." NEWLINE \
NEWLINE \
"  ExportWin3 control.ini_path" NEWLINE \
"    Export all color schemes to a CONTROL.INI file in the Windows 3 format." NEWLINE \
NEWLINE \
"  ApplyCurrentToNewUsers" NEWLINE \
"    Apply the current color scheme, fonts and sizes to the default user." NEWLINE

#define S_USAGE_EFF  S_EFF " actions:" NEWLINE \
NEWLINE \
"  EnableEffects" NEWLINE \
"    Enable all supported visual effects and animations." NEWLINE \
NEWLINE \
"  DisableEffects" NEWLINE \
"    Disable the visual effects above." NEWLINE \
NEWLINE \
"  EnableHighContrast" NEWLINE \
"    Enable High Contrast mode, without changing system colors." NEWLINE \
NEWLINE \
"  DisableHighContrast" NEWLINE \
"    Disable High Contrast mode, without changing system colors." NEWLINE \
NEWLINE \
"  RestoreDefaults" NEWLINE \
"    Restore default settings." NEWLINE

#define S_FILEPATH_INVALID  "The parameter is not a file path."

#define S_SCHEME             "Color scheme"
#define S_SIZES              "Sizes"
#define S_SCHEME_LOAD_ERROR  "Could not load the scheme." NEWLINE \
                             "Verify that the names are correct."
#define S_NO_SCHEMES         "No schemes found."

#define S_HIGHCONTRASTMODE  "High Contrast mode"
#define S_UNKNOWN           "Unknown"
#define S_ENABLED           "Enabled"
#define S_DISABLED          "Disabled"

#include "template.rc"
#include "undef.h"
