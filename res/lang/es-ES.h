#define RES_LANG     LANG_SPANISH
#define RES_SUBLANG  SUBLANG_SPANISH_MODERN


/* Menús */

#define S_PREVIEW_MENU_NORMAL    "&Normal"
#define S_PREVIEW_MENU_DISABLED  "&Deshabilitado"
#define S_PREVIEW_MENU_SELECTED  "&Seleccionado"

#define S_PREVIEW_MENU_CLASSIC  "Ventana de previsualización &clásica"


/* Cadenas de diálogos */

#define S_OK      "Aceptar"
#define S_CANCEL  "Cancelar"

#define S_CLASSIC                   "Estilo clásico"
#define S_CLASSIC_SCHEME            "&Combinación de colores:"
#define S_CLASSIC_SCHEMESIZE        "Ta&maño:"
#define S_CLASSIC_ELEMENT           "&Ítem:"
#define S_CLASSIC_SIZE1             "&Tamaño 1:"
#define S_CLASSIC_SIZE2             "Tamaño 2:"
#define S_CLASSIC_COLOR1            "Color &1:"
#define S_CLASSIC_COLOR2            "Color &2:"
#define S_CLASSIC_FONTNAME          "&Fuente:"
#define S_CLASSIC_FONTSIZE          "Tamaño:"
#define S_CLASSIC_FONTWIDTH         "Ancho:"
#define S_CLASSIC_FONTCOLOR         "Colo&r:"
#define S_CLASSIC_FONTANGLE         "Ángulo:"
#if defined(WITH_CLASSIC_ADVANCED_SETTINGS)
#define S_CLASSIC_FONTSTYLE         "Estilo fuente:"
#else
#define S_CLASSIC_FONTSTYLE         "Estilo:"
#endif
#define S_CLASSIC_FONTBOLD          "N"
#define S_CLASSIC_FONTITALIC        "K"
#define S_CLASSIC_FONTUNDERLINE     "S"
#define S_CLASSIC_FONTSMOOTHING     "Suavizado de bordes:"
#define S_CLASSIC_GRADIENTCAPTIONS  "&Gradiente en títulos"
#define S_CLASSIC_FLATMENUS         "Menús &planos"

#define S_SAVESCHEME    "Guardar combinación de colores"
#define S_RENAMESCHEME  "Renombrar combinación de colores"
#define S_DELETESCHEME  "Eliminar combinación de colores"
#define S_BOLD          "Negrita"
#define S_ITALIC        "Cursiva"
#define S_UNDERLINE     "Subrayado"

#define S_EFF                     "Efectos"
#define S_EFF_ICONS               "Íconos del escritorio"
#define S_EFF_ICONS_SIZE          "Tamaño:"
#define S_EFF_ICONS_XMARGIN       "Margen horizontal:"
#define S_EFF_ICONS_YMARGIN       "Margen vertical:"
#define S_EFF_ICONS_LABELSHADOW   "Sombras de etiquetas"
#define S_EFF_ICONS_TRANSSELRECT  "Rectángulo de selección translúcido"
#define S_EFF_ICONS_CUSTOMIZE     "Personalizar &íconos..."
#define S_EFF_EFF                 "Efectos visuales"
#define S_EFF_MASTER              "Habilitar efectos visuales"
#define S_EFF_ANIMATIONS_BUTTON   "&Animaciones..."
#define S_EFF_MENUSHADOW          "Sombras de menú"
#define S_EFF_MENUSHADOW_TT       "Si el compositor de ventanas (DWM) y un estilo moderno están habilitados, esto también activa sombras en las ventanas."
#define S_EFF_POINTERSHADOW       "Sombras de puntero"
#define S_EFF_DRAGFULLWIN         "Mostrar ventana al arrastrarla"
#if WINVER >= WINVER_XP
#define S_EFF_FONTSMOOTHING       "Suavizado de bordes de fuentes:"
#else
#define S_EFF_FONTSMOOTHING       "Suavizado de bordes de fuentes"
#endif
#define S_EFF_APPEARANCE          "Apariencia"
#define S_EFF_KEYBOARDCUES        "Siempre &subrayar métodos abreviados de teclado (Alt+tecla)"
#define S_EFF_KEYBOARDCUES_TT     "Si no se establece, los métodos abreviados de teclado de menú y otros elementos sólo se subrayan luego de apretar la tecla Alt."
#define S_EFF_RIGHTALIGNMENU      "Alinear menús a la derecha"
#define S_EFF_RIGHTALIGNMENU_TT   "Mostrar submenús y menús emergentes a la izquierda, si es posible."
#define S_EFF_HIGHCONTRAST        "Modo alto &contraste"
#define S_EFF_HIGHCONTRAST_TT     "Indicar a las aplicaciones que usen un modo de alto contraste. Esto no afecta a los colores del sistema."
#define S_EFF_RESET               "&Restaurar valores"

#define S_ANIM           "Animaciones"
#define S_ANIM_MENUOPEN  "Apertura de menú:"
#define S_ANIM_MENUSEL   "Selección de ítem de menú (desvanecer)"
#define S_ANIM_TOOLTIP   "Descripción emergente:"
#define S_ANIM_COMBOBOX  "Apertura de cuadros combo (deslizar)"
#define S_ANIM_LISTBOX   "Desplazamiento suave de cuadros de lista"
#define S_ANIM_WINDOW    "Minimizado, maximizado y restaurado de ventana"
#define S_ANIM_CONTROL   "Controles y elementos dentro de ventanas"
#define S_ANIM_FADE      "Desvanecer"
#define S_ANIM_SLIDE     "Deslizar"
#define S_ANIM_TYPE_TT   "Note que si el compositor de ventanas (DWM) está habilitado, las animaciones de deslizamiento pueden tener un fondo blanco."


/* Tablas de cadenas */

#define S_PROPSHEET_NAME    "Apariencia"
#define S_APP_DESCRIPTION   "Cambia la apariencia de su escritorio, incluyendo los colores, fuentes y tamaños."
#define S_APP_NAME          "Configurador clásico de apariencia"
#define S_CLASSIC_KEYWORDS  "estilo clásico;tema clásico;combinación de colores;fuentes;tamaños"
#define S_EFF_KEYWORDS      "efectos visuales;alto contraste;rendimiento"

#define S_ERROR          "Error"
#define S_ERROR_GENERIC  "Ha ocurrido un error"
#define S_ERROR_MEM      "No se pudo reservar memoria"
#define S_ERROR_UXTHEME  "No se pudieron cargar las funciones de UxTheme"
#define S_ERROR_THEME    "No se pudo aplicar el tema"
#define S_ERROR_MSSPATH  "No se puede aplicar un archivo de estilo en una carpeta del usuario"

#define S_CURRENT_ELEMENT  "(Actual)"

#define S_INACTIVE_CAPTION  "Ventana inactiva"
#define S_ACTIVE_CAPTION    "Ventana activa"
#define S_WINDOW_TEXT       "Texto de la ventana"
#define S_MESSAGE_CAPTION   "Cuadro de mensaje"
#define S_MESSAGE_TEXT      "Texto del mensaje"
#define S_MAIN_CAPTION      "Previsualización"
#define S_DISABLED_TEXT     "Texto deshabilitado"
#define S_NORMAL_TEXT       "Texto normal"
#define S_SELECTED_TEXT     "Texto seleccionado"
#define S_PALETTE_CAPTION   "Ventana de paleta"
#define S_BUTTON_TEXT       "Botón"
#define S_TOOLTIP_TEXT      "Descripción emergente."
#define S_ICON_LABEL        "Icono"

#define S_ELEMENT_0   "Escritorio"
#define S_ELEMENT_1   "Fondo de la aplicación"
#define S_ELEMENT_2   "Ventana"
#define S_ELEMENT_3   "Menú"
#define S_ELEMENT_4   "Barra de título activa"
#define S_ELEMENT_5   "Barra de título inactiva"
#define S_ELEMENT_6   "Barra de título de paleta"
#define S_ELEMENT_7   "Borde de ventana activa"
#define S_ELEMENT_8   "Borde de ventana inactiva"
#define S_ELEMENT_9   "Barra de desplazamiento"
#define S_ELEMENT_10  "Objeto 3D"
#define S_ELEMENT_11  "Sombra 3D"
#define S_ELEMENT_12  "Luz 3D"
#define S_ELEMENT_13  "Ítem seleccionado"
#define S_ELEMENT_14  "Ítem deshabilitado"
#define S_ELEMENT_15  "Descripción emergente"
#define S_ELEMENT_16  "Cuadro de mensaje"
#define S_ELEMENT_17  "Hipervínculo"
#define S_ELEMENT_18  "Barra de menú plana"
#define S_ELEMENT_19  "Borde con relleno"

#define S_FONTSMOOTHING_DEFAULT  "Predeterminado"
#define S_FONTSMOOTHING_OFF      "Deshabilitado"
#define S_FONTSMOOTHING_CT       "ClearType"
#define S_FONTSMOOTHING_CTN      "ClearType Natural"

#define S_SAVESCHEME_ERROR              "La combinación de colores no pudo ser guardada."
#define S_RENAMESCHEME_ERROR            "La combinación de colores no pudo ser renombrada."
#define S_RENAMESIZE_ERROR              "El tamaño no pudo ser renombrado."
#define S_SAVESCHEME_CONFIRM_OVERWRITE  "Ya existe una combinación de colores y tamaño con esos nombres." \
                                        NEWLINE "¿Desea sobreescribirla?"
#define S_RENAMESCHEME_CONFLICT         "Ya existe una combinación de colores con ese nombre."
#define S_RENAMESIZE_CONFLICT           "Ya existe un tamaño con ese nombre."
#define S_SAVESCHEME_NO_NAME            "Debe especificar el nombre de la combinación de colores."
#define S_SAVESCHEME_NO_SIZE            "Debe especificar el nombre del tamaño."
#define S_SAVESCHEME_DEFAULT_NAME       "Combinación personalizada"
#define S_SAVESCHEME_DEFAULT_SIZE       "Normal"
#define S_DELETE_CONFIRM_TITLE          "Confirmación de eliminado"
#define S_DELETESCHEME_CONFIRM_SCHEME   "¿Confirma que desea eliminar la combinación de colores seleccionada?"
#define S_DELETESCHEME_CONFIRM_SIZE     "¿Confirma que desea eliminar el tamaño seleccionado?"
#define S_DELETESCHEME_ERROR_SCHEME     "Ha ocurrido un error al intentar eliminar la combinación de colores."
#define S_DELETESCHEME_ERROR_SIZE       "Ha ocurrido un error al intentar eliminar el tamaño."

/* Ayuda de uso por línea de comandos */

#if defined(WITH_THEMES)
#define S_USAGE_GENERAL_THEMES \
"ruta_archivo_tema" NEWLINE \
"  Aplicar el archivo de tema especificado." NEWLINE \
NEWLINE
#else
#define S_USAGE_GENERAL_THEMES ""
#endif

/* No traduzca esto; son los nombres de argumentos de la línea de comandos */
#if defined(WITH_CLASSIC)
#define S_USAGE_PAGES_CLASSIC "  Appearance:  " S_CLASSIC NEWLINE
#else
#define S_USAGE_PAGES_CLASSIC ""
#endif
#if defined(WITH_EFF)
#define S_USAGE_PAGES_EFF     "  Effects:     " S_EFF NEWLINE
#else
#define S_USAGE_PAGES_EFF     ""
#endif

#define S_USAGE_GENERAL \
"Uso:" NEWLINE \
S_USAGE_GENERAL_THEMES \
"[@Página [/Acción [Parámetros]]]" NEWLINE \
"  Por ejemplo: @Appearance /Apply ""Windows Standard"" 1-Normal" NEWLINE \
"  Especifique una página sin parámetros adicionales para mostrarla." NEWLINE \
"  Especifique una página con /? para consultar su uso por línea de comandos." NEWLINE \
NEWLINE \
"Páginas:" NEWLINE \
S_USAGE_PAGES_CLASSIC \
S_USAGE_PAGES_EFF

#define S_USAGE_APPEARANCE  "Acciones de " S_CLASSIC NEWLINE \
NEWLINE \
"  List" NEWLINE \
"    Mostrar por consola una lista de todas las combinaciones de colores y" NEWLINE \
"    tamaños." NEWLINE \
NEWLINE \
"  Apply nombre_combinación_colores nombre_tamaño" NEWLINE \
"    Aplicar la combinación especificada." NEWLINE \
NEWLINE \
"  SaveCurrent nombre_combinación_colores nombre_tamaño" NEWLINE \
"    Guardar la combinación actual del sistema con los nombres especificados." NEWLINE \
NEWLINE \
"  ImportWin3 ruta_control.ini" NEWLINE \
"    Importar al sistema todas las combinaciones de colores en un archivo" NEWLINE \
"    CONTROL.INI de Windows 3." NEWLINE \
NEWLINE \
"  ExportSchemesToWin3 ruta_control.ini" NEWLINE \
"    Exportar todas las combinaciones de colores a un archivo CONTROL.INI con" NEWLINE \
"    el formato usado por Windows 3." NEWLINE \
NEWLINE \
"  ApplyCurrentToNewUsers" NEWLINE \
"    Aplicar la combinación de colores, fuentes y tamaños actual al usuario" NEWLINE \
"    predeterminado." NEWLINE

#define S_USAGE_EFF  "Acciones de " S_EFF NEWLINE \
NEWLINE \
"  EnableEffects" NEWLINE \
"    Habilitar todos los efectos visuales y animaciones soportadas." NEWLINE \
NEWLINE \
"  DisableEffects" NEWLINE \
"    Deshabilitar los efectos mencionados arriba." NEWLINE \
NEWLINE \
"  EnableHighContrast" NEWLINE \
"    Habililitar modo de alto contraste, sin cambiar los colores del sistema." NEWLINE \
NEWLINE \
"  DisableHighContrast" NEWLINE \
"    Deshabilitar modo de alto contraste, sin cambiar los colores del sistema." NEWLINE \
NEWLINE \
"  RestoreDefaults" NEWLINE \
"    Restaurar la configuración predeterminada." NEWLINE

#define S_FILEPATH_INVALID  "El parámetro no es una ruta de archivo."

#define S_SCHEME             "Combinación de colores"
#define S_SIZES              "Tamaños"
#define S_SCHEME_LOAD_ERROR  "No se pudo cargar la combinación." NEWLINE \
                             "Verifique que los nombres sean correctos."
#define S_NO_SCHEMES         "No se han encontrado combinaciones de colores."

#define S_HIGHCONTRASTMODE  "Modo alto contraste"
#define S_UNKNOWN           "Desconocido"
#define S_ENABLED           "Habilitado"
#define S_DISABLED          "Deshabilitado"

#include "template.rc"
#include "undef.h"
